/**
** This example blinks 8 LED in sequence
** All LED must be connect in sequence on 
** D port (D0 - D7)
**
** First program in Arduino by njrizzo
** on 2017-07-08
**/

int	data[8] = { 2,3,4,5,6,11,12,13 };
/*  Data is array with all pin of D port */

int   lastpin;
/*  keep the last pin was blinked, in loop */

void setup(){

int c;

	/* Start all Digital D port in LOW state */
	for(c=0; c < 8; c++){
		pinMode(data[c], OUTPUT);
		digitalWrite(data[c],LOW);
	}
	lastpin = 0;
}

void loop(){

	/* Turn On data[lastpin] */
	digitalWrite(data[lastpin],HIGH);
	delay(1000);
	/* Turn Off data[lastpin] */
	digitalWrite(data[lastpin],LOW);
	delay(1000);
	lastpin++
	/* if lastpin grater than 7 restart counter */
	if (lastpin >= 8)
		lastpin ^= lastpin;
}
